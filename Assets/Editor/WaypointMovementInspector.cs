﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaypointGroup))]
public class WaypointMovementInspector : Editor
{
    public override void OnInspectorGUI()
    {
        WaypointGroup way = (WaypointGroup) target;
        if (way == null) return;

        if(GUILayout.Button("New Waypoint", GUILayout.MaxWidth(Screen.width - 30), GUILayout.MaxHeight(60)))
        {
           Selection.activeGameObject = way.AddWaypoint();
        }

        GUILayout.Space(10);

        if(GUILayout.Button("Redo Listing", GUILayout.MaxWidth(Screen.width - 30), GUILayout.MaxHeight(40)))
        {
           way.RestartList();
        }
    }
}
