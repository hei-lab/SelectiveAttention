﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WaypointGroup : MonoBehaviour
{
    // Base speed for the character to move to each waypoint
    private const float BASE_WAYPOINT_SPEED = 0.5F;

    [HideInInspector]
    public List<WaypointNode> waypoints;
    private WaypointNode _lastNode;

    /// <summary>
    /// Used in the Inspector script, Adds a new waypoint to the scene.
    /// </summary>
    /// <returns> The waypoint game object created</returns>
    public GameObject AddWaypoint()
    {
        GameObject waypoint = new GameObject("Waypoint" + (waypoints.Count + 1));
        waypoint.transform.SetParent(transform);
        
        WaypointNode node = waypoint.AddComponent<WaypointNode>();
        node.SpeedToNext = 1f;
        _lastNode = node;

        waypoints.Clear();
        foreach(Transform t in transform)
        {
            WaypointNode n = t.GetComponent<WaypointNode>();
            waypoints.Add(n);
        }

        return node.gameObject;
    }

    /// <summary>
    /// Restarts the list with the child waypoints
    /// </summary>
    public void RestartList()
    {
        int count = 1;
        waypoints.Clear();
        foreach(Transform t in transform)
        {
            WaypointNode n = t.GetComponent<WaypointNode>();
            n.gameObject.name = "Waypoint" + count++;
            n.SpeedToNext = BASE_WAYPOINT_SPEED;
            waypoints.Add(n);
        }
    }

    public void OnDrawGizmos() 
    {
        for (int i = 0; i < waypoints.Count; i++)
        {
            WaypointNode node = waypoints[i];
#if UNITY_EDITOR
            UnityEditor.Handles.Label(node.transform.position, node.gameObject.name);
#endif
            if (waypoints.Count > 2)
            {
                if (i >= waypoints.Count - 1) return;
                Vector3 to = waypoints[i + 1].transform.position;
  
                Gizmos.color = Color.white;
                Gizmos.DrawLine(node.transform.position, to);
            }
        }
    }
}
