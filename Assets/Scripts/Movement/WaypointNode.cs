﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaypointNode : MonoBehaviour
{
    [SerializeField] private float _speedToNext;
    [SerializeField] private float _timeToStop;
    [SerializeField] bool _stop;
    public UnityEngine.Events.UnityEvent OnWaypointReached;

    public bool Counted{get; set;}
    public bool Stop 
    {
        get
        {
            return _stop ^ Counted;
        }
    }
    public float TimeToStop => _timeToStop;
    public float SpeedToNext 
    {
        get
        {
            return _speedToNext;
        } 
        set
        {
            _speedToNext = value;
        }
    }
}
