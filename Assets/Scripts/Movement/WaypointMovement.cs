﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMovement : MonoBehaviour
{
    /// <summary>
    /// Group of waypoints to follow
    /// </summary>
    [SerializeField] private WaypointGroup _waypointGroup = default;
    /// <summary>
    /// Should the object rotate thowards the next waypoint
    /// </summary>
    [SerializeField] private bool _updateRotation = true;

    /// <summary>
    /// Current waypoint node index
    /// </summary>
    private int _currentIndex;
    /// <summary>
    /// Current direction the object is moving thowards
    /// </summary>
    private Vector3 _currentDir;
    /// <summary>
    /// The node the object is moving thowards
    /// </summary>
    private WaypointNode _currentNode;

    /// <summary>
    /// If the timer is counting
    /// </summary>
    private bool _isCounting;
    /// <summary>
    /// The current timer time
    /// </summary>
    private float _stopTimer;

    private void Start()
    {
        _currentIndex = 0;
        _currentNode = _waypointGroup.waypoints[_currentIndex];
        _currentDir = _currentNode.transform.position - transform.position;
    }

    private void Update()
    {
        Vector3 newPos = transform.position;

        //* Don't update position if the waypoint is counting down a timer
        if (!_isCounting)
        {
            // Update position
            //Get the direction
            newPos += _currentDir * Time.deltaTime * _currentNode.SpeedToNext;
            // Assign new position
            transform.position = newPos;
        }

        if (Vector3.Distance(transform.position, _currentNode.transform.position) <= 0.2)
        {
            // Set timer to start counting
            if (_currentNode.Stop)
            {
                _isCounting = true;
                _stopTimer = _currentNode.TimeToStop;
            }

            _currentNode.OnWaypointReached?.Invoke();

            if (!_isCounting)
            {
                // Get a new index and a new direction
                _currentIndex = NextIndex();
                _currentDir = _currentNode.transform.position - transform.position;
            }
        }

        if (_updateRotation)
        {
            UpdateRotation();
        }
        // Update Timer
        if (_isCounting)
        {
            _currentNode.Counted = true;
            _stopTimer -= Time.deltaTime;
            if (_stopTimer < 0.0f)
            {
                _isCounting = false;
            }
        }
    }

    private void UpdateRotation()
    {
        Vector3 newRot = Vector3.RotateTowards(transform.forward, _currentDir, Time.deltaTime * 1f, 0.0f);
        transform.rotation = Quaternion.LookRotation(newRot);
    }

    private int NextIndex()
    {
        int i = _currentIndex;
        i = i < _waypointGroup.waypoints.Count - 1 ? i + 1 : i;
        _currentNode = _waypointGroup.waypoints[_currentIndex];
        return i;
    }
}
