﻿using UnityEngine;

/// <summary>
/// Extension of the class Bounds.
/// </summary>
public static class MyBoundsExtensions
{
    /// <summary>
    /// Extension method that returns a random position in collider bounds.
    /// </summary>
    /// <param name="bounds">Extension variable</param>
    /// <param name="xInnerMargin">Inner X margin of the collider</param>
    /// <param name="yInnerMargin">Inner Y margin of the collider</param>
    /// <param name="zInnerMargin">Inner Z margin of the collider</param>
    /// <param name="useX">Bool to use or not X min and max positions</param>
    /// <param name="useY">Bool to use or not Y min and max positions</param>
    /// <param name="useZ">Bool to use or not Z min and max positions</param>
    /// <returns></returns>
    public static Vector3 RandomPositionInBounds(
        this Bounds bounds, 
        float xInnerMargin = 0f, 
        float yInnerMargin = 0f, 
        float zInnerMargin = 0f,
        bool useX = true,
        bool useY = true,
        bool useZ = true)
    {
        // Create a Vector3 with a random position inside the given collider
        // considering the margin given
        Vector3 randomPos = new Vector3(
            Random.Range(bounds.min.x + xInnerMargin, bounds.max.x - xInnerMargin),
            Random.Range(bounds.min.y + yInnerMargin, bounds.max.y - yInnerMargin),
            Random.Range(bounds.min.z + zInnerMargin, bounds.max.z - zInnerMargin));

        // If we choose not to use a position of the collider, set it to the center
        if (!useX) randomPos.x = bounds.center.x;
        if (!useY) randomPos.y = bounds.center.y;
        if (!useZ) randomPos.z = bounds.center.z;

        return randomPos;
    }
}