﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FormManager : MonoBehaviour
{
    [SerializeField] private GameObject[] questions = default;
    [SerializeField] private GameObject form = default;

    private DatabaseManager db;
    private List<string> answers;

    private void Awake()
    {
        db = FindObjectOfType<DatabaseManager>();
        answers = new List<string>();
    }

    private void Start()
    {
        List<string> q = db.RetrieveQuestions();

        for (int i = 0; i < questions.Length; i++)
        {
            questions[i].transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = q[i];
        }
    }

    public void SubmitValue(int i)
    {
        db.CreateAnswer(i + 1, questions[i].transform.GetChild(1).GetComponent<TMP_InputField>().text);   
    }

    public void EndSession()
    {
        db.EndSession();
        form.SetActive(false);
    }
}
