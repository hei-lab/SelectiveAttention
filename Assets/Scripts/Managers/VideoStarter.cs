﻿using System.Collections;
using UnityEngine;
using UnityEngine.Video;

public class VideoStarter : MonoBehaviour
{
    /// <summary>
    /// Raw image being used for the output of the video
    /// </summary>
    [SerializeField] GameObject _outputObject;
    [SerializeField] UnityEngine.Events.UnityEvent OnVideoComplete;
    private VideoPlayer _videoPlayer;

    void Start()
    {
        _videoPlayer = GetComponent<VideoPlayer>();
    }

    public void StartVideo()
    {
        if (!_outputObject.activeInHierarchy)
            _outputObject.SetActive(true);
        
        if (_videoPlayer.isPrepared)
        {
            _videoPlayer.Play();
        }
        else
        {
            _videoPlayer.Prepare();
        }
        StartCoroutine(WaitForEndOfVideo());
    }

    public void HideVideo()
    {
        _outputObject.SetActive(false);
        _videoPlayer.Stop();
    }

    private  IEnumerator WaitForEndOfVideo()
    {
        while(_videoPlayer.isPlaying)
        {
            yield return null;
        }
        Debug.Log("Video Finished");
        OnVideoComplete?.Invoke();
    }
}
