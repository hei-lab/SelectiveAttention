﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

public class SessionRecorder
{
    private const string EXTENSION = ".csv";
    private string filepath;
    StreamWriter file;
    private bool isInitialized = false;

    public SessionRecorder()
    {
        filepath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        filepath = Path.Combine(filepath, @"Selective Attention\Sessions");
    }

    public void InitializeSession()
    {
        isInitialized = true;

        string fileName;
        DateTime time = DateTime.Now;
        string hour = null;
        string minute = null;

        Directory.CreateDirectory(filepath);

        hour = FormatSingleNumber(time.Hour);
        minute = FormatSingleNumber(time.Minute);

        fileName = $"SelAtt_{time.Day}-{time.Month}-{time.Year}_{FormatSingleNumber(time.Hour)}h{FormatSingleNumber(time.Minute)}m";

        file = new StreamWriter(Path.Combine(filepath, fileName + EXTENSION), true, Encoding.UTF8);
    }

    // If hour/minute is a single digit, format it
    // Example: 5h6m now becomes 05h06m 
    private string FormatSingleNumber(int num)
    {
        string returnVal;
        string value = num.ToString();

        if (value.Length == 1)
            returnVal = "0" + value;
        else
            returnVal = value;

        return returnVal;
    }

    public void AddQuestionList(List<string> questions)
    {
        file.WriteLine();
        file.WriteLine($"Lista de perguntas:");

        for (int i = 0; i < questions.Count; i++)
        {
            string s = $"{i + 1} - {questions[i]}";
            file.WriteLine("\"" + s + "\"");
        }
        
        file.WriteLine();
        file.WriteLine();
        file.WriteLine();
    }

    public void AddUser(string playerCode, string teamColor, string date)
    {
        file.WriteLine();
        file.WriteLine();
        file.WriteLine($"Código de Utilizador: {playerCode}");
        file.WriteLine($"Cor da equipa para contar passes: {teamColor}");
        file.WriteLine($"Data: {date}");
        file.WriteLine();
        file.WriteLine($"\"Pergunta\",\"Resposta\"");
        file.WriteLine();
    }
    public void AddRecord(string question, string answer)
    {
        if (isInitialized)
        {
            file.WriteLine($"\"{question}\",\"{answer}\"");
        }
    }

    public void EndSession()
    {
        file.Close();
    }
}
