﻿using System;
using MySql.Data.MySqlClient;
using UnityEngine;
using System.Collections.Generic;


public class DatabaseManager : MonoBehaviour
{
    [SerializeField] private DatabaseConnectionParams connParams;

    private const int DEFAULT_PORT = 3306;

    private string tableAnswers = "Answer";
    private string tableQuestions = "Question";
    private string tableUsers = "User";

    private int userId;
    private bool isUserCreated;

    private MySqlConnection connection;
    private SessionRecorder sessionRec = new SessionRecorder();
    private static DatabaseManager instance;

    public static DatabaseManager Instance => instance;

    public void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        Connect();
    }

    public void Connect()
    {
        if (connection != null) return;

        string connectionStr = "";

        connectionStr += $"server={connParams.dataSource};";
        connectionStr += $"database={connParams.databaseName};";
        connectionStr += $"user={connParams.username};";
        connectionStr += $"port={DEFAULT_PORT};";
        connectionStr += $"password={connParams.password};";

        connection = new MySqlConnection(connectionStr);
        try
        {
            Debug.Log("Connecting...");
            connection.Open();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to connect: " + e.Message);
        }

        if (connection != null) Debug.Log("Connected!");
    }

    public void CreateUser(string userCode, string shirtColor)
    {
        DateTime date = DateTime.Now;
        string dateStr = $"{FormatSingleNumber(date.Day)}-" +
            $"{FormatSingleNumber(date.Month)}-" +
            $"{FormatSingleNumber(date.Year)}";

        string createUserQuery = "";

        createUserQuery += $"INSERT INTO {tableUsers} VALUES (";
        createUserQuery += "NULL,";
        createUserQuery += $"'{userCode}', ";
        createUserQuery += $"'{dateStr}', ";
        createUserQuery += $"'{shirtColor}'";
        createUserQuery += ");";

        RunQuery(createUserQuery);

        userId = RetrieveLastUserID();

        isUserCreated = true;
    }

    public int RetrieveLastUserID()
    {
        int user = default;
        MySqlDataReader dataReader = null;

        RunQuery($"SELECT MAX(ID) AS LastID FROM {tableUsers};", ref dataReader);
        {
            while (dataReader.Read())
            {
                user = dataReader.GetInt32(dataReader.GetOrdinal("LastID"));
                break;
            }
        }

        if (dataReader != null) dataReader.Close();
        return user;
    }

    public List<string> RetrieveQuestions()
    {
        MySqlDataReader dataReader = null;
        List<string> questions = new List<string>();

        RunQuery($"SELECT * FROM {tableQuestions};", ref dataReader);
        {
            while (dataReader.Read())
            {
                questions.Add(dataReader.GetString(dataReader.GetOrdinal("Question")));
            }
        }

        if (dataReader != null) dataReader.Close();

        return questions;
    }

    public void CreateAnswer(int questionId, string answer)
    {
        if (!isUserCreated) return;

        string createDataQuery = "";

        createDataQuery += $"INSERT INTO {tableAnswers} VALUES (";
        createDataQuery += "NULL,";
        createDataQuery += $"{userId}, ";
        createDataQuery += $"{questionId}, ";
        createDataQuery += $"'{answer}'";
        createDataQuery += ");";

        RunQuery(createDataQuery);
    }

    private (string, string, string) RetrieveUserData(int id)
    {
        MySqlDataReader dataReader = null;
        (string, string, string) userData = default;

        RunQuery($"SELECT * FROM {tableUsers} WHERE ID = {id};", ref dataReader);
        {
            while (dataReader.Read())
            {
                userData.Item1 = dataReader.GetString(dataReader.GetOrdinal("UserCode"));
                userData.Item2 = dataReader.GetString(dataReader.GetOrdinal("ShirtColor"));
                userData.Item3 = dataReader.GetString(dataReader.GetOrdinal("Date"));
            }
        }

        if (dataReader != null) dataReader.Close();

        return userData;
    }

    private List<(int, string)> RetrieveAnswerData(int id)
    {
        MySqlDataReader dataReader = null;
        List<(int, string)> answerData = new List<(int, string)>();
        (int, string) currentAnswer = default;

        RunQuery($"SELECT * FROM {tableAnswers} WHERE UserID = {id};", ref dataReader);
        {
            while (dataReader.Read())
            {
                currentAnswer.Item1 = dataReader.GetInt32(dataReader.GetOrdinal("QuestionID"));
                currentAnswer.Item2 = dataReader.GetString(dataReader.GetOrdinal("Answer"));
                answerData.Add(currentAnswer);
            }
        }

        if (dataReader != null) dataReader.Close();

        return answerData;
    }

    public void ExportData()
    {
        int lastUserId = RetrieveLastUserID();
        (string, string, string) userData;
        List<(int, string)> answerData = new List<(int, string)>();

        sessionRec.InitializeSession();
        sessionRec.AddQuestionList(RetrieveQuestions());

        for (int i = 1; i <= lastUserId; i++)
        {
            userData = RetrieveUserData(i);
            answerData = RetrieveAnswerData(i);
            sessionRec.AddUser(userData.Item1, userData.Item2, userData.Item3);

            foreach ((int, string) a in answerData)
            {
            sessionRec.AddRecord(a.Item1.ToString(), a.Item2);
            }
        }

        sessionRec.EndSession();
        EndSession();
    }

    public void EndSession()
    {
        if (!isUserCreated) return;
        connection.Close();
    }

    private void RunQuery(string query)
    {
        MySqlCommand sql = new MySqlCommand(query, connection);
        try
        {
            sql.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
        }

        Debug.Log(query);
    }

    private void RunQuery(string query, ref MySqlDataReader reader)
    {
        MySqlCommand sql = new MySqlCommand(query, connection);
        try
        {
            reader = sql.ExecuteReader();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
        }

        Debug.Log(query);
    }

    private string FormatSingleNumber(int num)
    {
        string returnVal;
        string value = num.ToString();

        if (value.Length == 1)
            returnVal = "0" + value;
        else
            returnVal = value;

        return returnVal;
    }
}
