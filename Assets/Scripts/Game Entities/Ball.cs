﻿using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public void TravelTo(Npc target, float throwSpeed, float throwAngle, float gravity)
    {
        transform.SetParent(null);
        StartCoroutine(LeapTowards(target, throwSpeed, throwAngle, gravity));
    }
    private IEnumerator LeapTowards(Npc target, float throwSpeed, float throwAngle, float gravity)
    {
        Transform targ = target.BallHolder;
        float distance = Vector3.Distance(transform.position, targ.position);

        // Get parabola velocities
        float parabVelocity = distance / (Mathf.Sin(2 * throwAngle * Mathf.Deg2Rad) / gravity);
        float velZ = Mathf.Sqrt(parabVelocity) * Mathf.Cos(throwAngle * Mathf.Deg2Rad);
        float velY = Mathf.Sqrt(parabVelocity) * Mathf.Sin(throwAngle * Mathf.Deg2Rad);

        float ballFlightDuration = distance / velZ;
        float elapsedTime = 0;

        transform.LookAt(targ);

        while (elapsedTime < ballFlightDuration)
        {
            transform.Translate(0, (velY - (gravity * elapsedTime)) * throwSpeed * Time.deltaTime, velZ * Time.deltaTime);

            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = targ.position;
        transform.SetParent(targ);
    }
}
