﻿using UnityEngine;
using System.Collections;

public class TeamWalkManager : MonoBehaviour
{
    [SerializeField] private float wanderingRadius = default;
    [SerializeField] private float nonWalkableCenterRadius = default;
    [SerializeField] private float minWalkTimer = default;
    [SerializeField] private float maxWalkTimer = default;
    [SerializeField] private float timeForWalking = default;

    private SphereCollider col;
    private Npc[] players;
    private float t = 0;

    private void Awake()
    {
        col = GetComponent<SphereCollider>();
        col.radius = wanderingRadius;
        players = GetComponentsInChildren<Npc>();

        foreach (Npc npc in players)
            npc.WalkTimer = Random.Range(minWalkTimer, maxWalkTimer);
    }

    private void Start()
    {
        TeamWalk();
    }

    private void Update()
    {
        t += Time.deltaTime;
    }

    private void TeamWalk()
    {
        foreach (Npc npc in players)
            StartCoroutine(NpcWalkTimer(npc, npc.WalkTimer));
    }

    private IEnumerator NpcWalkTimer(Npc npc, float walkTimer)
    {
        yield return new WaitForSeconds(walkTimer);

        if (!npc.IsPassing)
        {
            Vector3 destPos = col.bounds.RandomPositionInBounds
                (xInnerMargin: 0.5f, zInnerMargin: 0.5f, useY: false);
            npc.WalkTo(destPos);
        }

        if (t < timeForWalking)
            StartCoroutine(NpcWalkTimer(npc, walkTimer));
    }
}
