/// <summary>
/// Defines a player team
/// </summary>
public enum Team
{
    WHITE,
    BLACK
}