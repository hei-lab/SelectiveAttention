﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TeamPassesManager : MonoBehaviour
{
    [SerializeField] private int _whiteTeamPasses = default;
    [SerializeField] private int _blackTeamPasses = default;
    [SerializeField] private float _timeForPasses = default;
    [SerializeField] private UnityEngine.Events.UnityEvent OnPassesEnd;

    private Npc[] _whiteTeam;
    private Npc[] _blackTeam;
    /// <summary>
    /// Npc in each team that has the ball
    /// </summary>
    private Npc _activeWhite, _activeBlack;
    private float _passesPerSecondWhiteTeam;
    private float _passesPerSecondBlackTeam;
    private WaitForSeconds _whiteTeamWait;
    private WaitForSeconds _blackTeamWait;

    private void Awake() 
    {
        _whiteTeam = GetComponentsInChildren<Npc>().
            Where(x => x.BelongingTeam.Equals(Team.WHITE)).ToArray();
        
        _blackTeam = GetComponentsInChildren<Npc>().
            Where(x => x.BelongingTeam.Equals(Team.BLACK)).ToArray();

        //* debuggin the teams
        foreach (Npc npc in _whiteTeam)
        {
            if (npc.HasBall()) _activeWhite = npc;
        }
        foreach (Npc npc in _blackTeam)
        {
            if (npc.HasBall()) _activeBlack = npc;
        }

        // Debug.Log(_whiteTeam.Length);
        // Debug.Log(_blackTeam.Length);

        //* Velocity in which the teams pass the ball
        _passesPerSecondWhiteTeam = _timeForPasses / _whiteTeamPasses;
        _passesPerSecondBlackTeam = _timeForPasses / _blackTeamPasses;

        _whiteTeamWait = new WaitForSeconds(_passesPerSecondWhiteTeam);
        _blackTeamWait = new WaitForSeconds(_passesPerSecondBlackTeam);

        Debug.Log("Black team passes per second: " + _passesPerSecondBlackTeam);
        Debug.Log("White team passes per second: " + _passesPerSecondWhiteTeam);
    }

    // Starts the black and white team passes
    private void Start() 
    {
        StartCoroutine(TeamPass(_whiteTeam));    
        StartCoroutine(TeamPass(_blackTeam));
    }

    private IEnumerator TeamPass(Npc[] team)
    {
        float initTimeStamp = Time.time;
        float t = 0;
        while(t < _timeForPasses)
        {
            t = Time.time - initTimeStamp;
            Npc target = GetNewTarget(team);

            if (target.BelongingTeam.Equals(Team.WHITE))
            {
                StartCoroutine(_activeWhite.RotateTowards(target.transform));
                StartCoroutine(target.RotateTowards(_activeWhite.transform));
                target.Anim.SetTrigger("Receiving");
                _activeWhite.Stop();
                target.Stop();
                _activeWhite.Throw(target);
                _activeWhite = target;
                yield return _whiteTeamWait;
            }
            else
            {
                StartCoroutine(_activeBlack.RotateTowards(target.transform));
                StartCoroutine(target.RotateTowards(_activeBlack.transform));
                target.Anim.SetTrigger("Receiving");
                _activeBlack.Stop();
                target.Stop();
                _activeBlack.Throw(target);
                _activeBlack = target;
                yield return _blackTeamWait;
            }
        }

        OnPassesEnd?.Invoke();
    }

    private Npc GetNewTarget(Npc[] team)
    {
        Npc newTarget = team[Random.Range(0, team.Length)];
        
        if (newTarget.HasBall()) return GetNewTarget(team);
        return newTarget;
    }


}
