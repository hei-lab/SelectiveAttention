﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Npc : MonoBehaviour
{
    [Header("Ball properties")]
    [SerializeField] private Transform ballHolder = default;
    [SerializeField] private float throwSpeed = default;
    [SerializeField] private float throwAngle = default;
    [SerializeField] private float ballGravity = default;

    [Header("NPC Properties")]
    [SerializeField] private Team npcTeam = default;
    [SerializeField] private float rotationSpeed = default;
    [SerializeField] private float degreeStep = default;
    [SerializeField] private float rotationTimer = default;

    private Npc target;
    private Animator anim;
    private bool isMoving;
    private Vector3 destTarget;

    public Transform BallHolder => ballHolder;
    public Team BelongingTeam => npcTeam;
    public NavMeshAgent Agent { get; private set; }
    public float WalkTimer { get; set; }
    public bool IsPassing { get; set; }

    public Animator Anim => anim;

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }

    public void Throw(Npc ballTarget)
    {
        target = ballTarget;
        IsPassing = true;
        target.IsPassing = true;

        if (HasBall())
            if (!target.HasBall())
                anim.SetTrigger("Throw");
    }

    public IEnumerator RotateTowards(Transform target)
    {
        Vector3 lookDir = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(lookDir, transform.up);
        float t = 0;

        while (t < rotationTimer)
        {
            t += Time.deltaTime;

            transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed * degreeStep);
            yield return null;
        }
    }

    private void Update()
    {
        if (isMoving)
        {
            if (Vector3.Distance(transform.position, destTarget) < Agent.stoppingDistance)
            {
                Stop();
            }
        }
    }

    public void WalkTo(Vector3 dest)
    {
        Agent.isStopped = false;
        Agent.SetDestination(dest);
        isMoving = true;
        destTarget = dest;
        anim.SetBool("Moving", true);
    }

    public void Stop()
    {
        isMoving = false;
        Agent.isStopped = true;
        anim.SetBool("Moving", false);
    }

    // Called in the throw animation (Using Throw_Rui for now since the other is a bit more glitchy)
    public void ThrowAnimEvent()
    {
        Ball ball = ballHolder.GetChild(0).GetComponent<Ball>();
        ball.TravelTo(target, throwSpeed, throwAngle, ballGravity);
        IsPassing = false;
    }

    // Check if the ball holder object inside the npc right hand has a ball
    public bool HasBall() => (ballHolder.childCount > 0) ? true : false;
}
