﻿using System.Collections;
using UnityEngine;
using System;

/// <summary>
/// Controls the animations of the gorilla
/// </summary>
public class GorillaAnimControl : MonoBehaviour
{
    private Animator _anim;

    private void Awake() 
    {
        _anim = GetComponent<Animator>();    
    }

    public void Idle(float time)
    {
        _anim.SetBool("Idle", true);
        StartCoroutine(SwitchAfterTime(time, () => _anim.SetBool("Idle", false)));
    }

    private IEnumerator SwitchAfterTime(float time, Action onEnd)
    {
        yield return new WaitForSeconds(time);
        onEnd?.Invoke();
    }
}
