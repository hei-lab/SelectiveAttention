﻿using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Auth", menuName = "Authentication/Auth Object")]
public class Auth : ScriptableObject
{
    [SerializeField] private string userAuth = "";
    [SerializeField] private string adminAuth = "";
    [SerializeField] private ExpiryDate expiryDate;


    [System.Serializable]
    private struct ExpiryDate
    {
        public int Day;
        public int Month;
        public int Year;
        public int Hour;
        public int Minute;
    }

    public DateTime ExpDate => new DateTime(expiryDate.Year, expiryDate.Month, expiryDate.Day, expiryDate.Hour, expiryDate.Minute, 0);
    public string UserAuth => userAuth;
    public string AdminAuth => adminAuth;

    public string UserCode { get; private set;}

    public void SetUserCode(string s)
    {
        UserCode = s;
    }
}
