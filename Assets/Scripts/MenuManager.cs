﻿using UnityEngine;
using TMPro;
using System;

public class MenuManager : MonoBehaviour
{
    [Header("Authentication")]
    [SerializeField] private Auth auth;
    [SerializeField] private ConditionSet condition;

    [Header("Main Menu UI Elements")]
    [SerializeField] private TMP_InputField authTxt;
    [SerializeField] private TextMeshProUGUI wrongAuth;
    [SerializeField] private TMP_InputField codeTxt;
    [SerializeField] private GameObject adminPanel;
    [SerializeField] private GameObject authPanel;
    [SerializeField] private TextMeshProUGUI instructionsBody;
    [SerializeField] private TextMeshProUGUI exportText;

    private DatabaseManager db;
    private DateTime expiryDate;
    private DateTime currentDate = DateTime.Now;
    private SessionRecorder sessionRecorder;

    private void Awake()
    {
        expiryDate = auth.ExpDate;
        db = FindObjectOfType<DatabaseManager>();
    }

    public void Auth()
    {
        if (currentDate > expiryDate)
        {
            wrongAuth.text = "Credencial expirada!";
        }
        else
        {
            if (authTxt.text == auth.AdminAuth)
            {
                adminPanel.SetActive(true);
                authPanel.SetActive(false);
            }
            else if (authTxt.text == auth.UserAuth)
            {
                authPanel.SetActive(false);
            }
            else
            {
                wrongAuth.text = "Credencial errada!";
            }
        }
    }

    public void UserLogin()
    {
        auth.SetUserCode(codeTxt.text);
        condition.SetCondition();

        db.CreateUser(auth.UserCode, condition.ShirtColor.ToString());
    }

    public void SetInstructionsBody()
    {
        if (condition.ShirtColor == ConditionSet.Shirt.Black)
        {
            instructionsBody.text = "Conta as vezes que os jogadores de camisola preta passam a bola de basket entre si.";
        }
        else
        {
            instructionsBody.text = "Conta as vezes que os jogadores de camisola branca passam a bola de basket entre si.";
        }
    }

    public void ExportData()
    {
        db.ExportData();
        exportText.text = "Done!";
    }
}
