﻿using UnityEngine;

[CreateAssetMenu(fileName = "Condition")]
public class ConditionSet : ScriptableObject
{
    public Shirt ShirtColor { get; private set; }
    public void SetCondition()
    {
        ShirtColor = (Random.Range(0, 2) == 0) ? Shirt.Black : Shirt.White;
        Debug.Log(ShirtColor);
    }

    public enum Shirt
    {
        Black,
        White
    }
}
